import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

carsFile = pd.read_csv('./LV3/resources/mtcars.csv')

cylindar4= carsFile[carsFile.cyl == 4]  
cylindar6= carsFile[carsFile.cyl == 6]
cylindar8= carsFile[carsFile.cyl == 8]

index1=np.arange(0,cylindar4.car.count(),1)
index2=np.arange(0,cylindar6.car.count(),1) 
index3=np.arange(0,cylindar8.car.count(),1)




graphColumnWidth = 0.25

#2.1 Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
plt.figure()
plt.bar(index1, cylindar4["mpg"],graphColumnWidth, color=(1,0,0)) 
plt.bar(index2+graphColumnWidth, cylindar6["mpg"],graphColumnWidth, color=(0,1,0)) #Mora biti pomaknuto za 1 širinu
plt.bar(index2 + 2*graphColumnWidth, cylindar6["mpg"],graphColumnWidth, color=(0,0,1)) #Mora biti pomaknuto za 2 širine
plt.title("Potrosnja automobila s 4, 6 i 8 cilindara")
plt.xlabel('Car')
plt.ylabel('MpG')
plt.grid(linestyle='-')
plt.legend(['4 cilindra','6 cilindara','8 cilindara'],loc=2) 

#2.2 Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara. 

weight4=[]
weight6=[]
weight8=[]
for i in cylindar4["wt"]:
    weight4.append(i)
for i in cylindar6["wt"]:
    weight6.append(i)
for i in cylindar8["wt"]:
    weight8.append(i)
    
plt.figure()
plt.boxplot([weight4, weight6, weight8], positions = [4,6,8]) 
plt.title("Tezina automobila s 4, 6 i 8 cilindara")
plt.xlabel('Broj klipova')
plt.ylabel('Tezina u lbs')
plt.grid(linestyle='-')

#2.3 Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li 
#    automobili s ručnim mjenjačem veću potrošnju od automobila s automatskim mjenjačem? 

carsWithManualTransmision = carsFile[carsFile.am == 0]
carsWithAutomaticTransmision = carsFile[carsFile.am == 1]
carsWithManualMean = carsWithManualTransmision.mpg.mean()
carsWithAutomaticMean = carsWithAutomaticTransmision.mpg.mean()

indexManual=np.arange(0,1,1)
indexAutomatic=np.arange(0,1,1)

plt.figure()
plt.bar(indexManual, carsWithManualMean,graphColumnWidth, color=(1,0,0)) 
plt.bar(indexAutomatic+1, carsWithAutomaticMean,graphColumnWidth, color=(0,1,0)) 

plt.title("Potrosnja automobila s A ili M mjenjačem")
plt.xlabel('Mjenjač')
plt.ylabel('MpG')
plt.grid(linestyle='-')
plt.legend(['Manual','Automatski'],loc=2) 

#2.4 Prikažite na istoj slici odnos ubrzanja i snage automobila
#    za automobile s ručnim odnosno automatskim mjenjačem. 

Automatic_speed = []
Automatic_HP = []
Manual_speed = []
Manual_HP = []

for i in carsWithAutomaticTransmision["qsec"]:
    Automatic_speed.append(i)
for i in carsWithAutomaticTransmision["hp"]:
    Automatic_HP.append(i)
for i in carsWithManualTransmision["qsec"]:
    Manual_speed.append(i)
for i in carsWithManualTransmision["hp"]:
    Manual_HP.append(i)
    
plt.figure()
plt.scatter(Automatic_speed,Automatic_HP, marker="^")
plt.scatter(Manual_speed,Manual_HP,marker = "d")
plt.xlabel('Ubrzanje')
plt.ylabel('Snaga')
plt.grid(linestyle='-')
plt.legend(['Manual','Automatski'],loc=1) 