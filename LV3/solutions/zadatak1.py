import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

carsFile = pd.read_csv('./LV3/resources/mtcars.csv')

print(carsFile.head(4))
#1.1 Kojih 5 automobila ima najveću potrošnju? (koristite funkciju sort)
sortedByConsumption= carsFile.sort_values(by=['mpg'], ascending=False) 
print("5 automobila sa najvećom potrošnjom:")
print(sortedByConsumption['car'].head(5))
print("\n")

#1.2 Koja tri automobila s 8 cilindara imaju najmanju potrošnju? 
carsWith8Cylinders = carsFile[carsFile.cyl==8]
sortedByLeastConsumption = carsWith8Cylinders.sort_values(by=['mpg'], ascending=True)
print("3 automobila s 8 cilindara i najmanjom potrosnjom")
print(sortedByLeastConsumption['car'].head(3))
print("\n")

#1.3 Kolika je srednja potrošnja automobila sa 6 cilindara? 
carsWith6Cylinders = carsFile[carsFile.cyl==6]
mean6Cylinders = carsWith6Cylinders['mpg'].mean()
print("Srednja potrošnja automobila s 6 cilindara je : " + str(mean6Cylinders))
print("\n")

#1.4 Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs?
carsWith4Cylinders = carsFile[carsFile.cyl == 4]
carsInWeightRange = carsWith4Cylinders[(carsWith4Cylinders.wt >=2) & (carsWith4Cylinders.wt <=2.2)]
mean4Cylinders = carsWith4Cylinders['mpg'].mean()
print("Srednja potrošnja automobila s 4 cilindara(2000-2200) je : " + str(mean4Cylinders))
print("\n")

#1.5 Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka?
carsWithManualTransmision = carsFile[carsFile.am == 0]
carsWithAutomaticTransmision = carsFile[carsFile.am == 1]
print("Broj automobila s ručnim mjenjačem: "+ str(carsWithManualTransmision.car.count()))
print("Broj automobila s automatskim mjenjačem: "+ str(carsWithAutomaticTransmision.car.count()))
print("\n")

#1.6 Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga?
carsWithGreaterHP = carsWithAutomaticTransmision[carsWithAutomaticTransmision.hp > 100]
print("Broj auta s automatskim mjenjačem i HP > 100: "+ str(carsWithGreaterHP.car.count()))
print("\n")

#1.7  Kolika je masa svakog automobila u kilogramima?
carsWeightData = carsFile[['car','wt']]
carsWeightData.wt = carsWeightData.wt*454
print(carsWeightData)