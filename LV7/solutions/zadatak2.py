from keras.preprocessing.image import img_to_array
from keras.models import load_model
from matplotlib import pyplot as plt
from skimage.transform import resize
from skimage import color
import matplotlib.image as mpimg
import numpy as np

filename = 'sample_data/test.png'

img = mpimg.imread(filename)
img = color.rgb2gray(img)
img = resize(img, (28, 28))


plt.figure()
plt.imshow(img, cmap=plt.get_cmap('gray'))
plt.show()


img = img.reshape(1, 28, 28, 1)
img = img.astype('float32')

# TODO: ucitaj model
loaded_model = keras.models.load_model("sample_data/model")

# TODO: napravi predikciju
img_predicted = loaded_model.predict(
    img, batch_size=None, verbose=0, steps=None, callbacks=None, max_queue_size=10,
    workers=1, use_multiprocessing=False)[0]

# TODO: ispis rezultat
print("------------------------")
img_predicted[img_predicted>=0.5] = 1
img_predicted[img_predicted<0.5] = 0

print(np.where(img_predicted == 1)[0])