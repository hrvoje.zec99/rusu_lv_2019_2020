import re
import numpy as np
import matplotlib.pyplot as plt

genders = np.random.randint(2, size = 12)
gauss = np.random.randint(1, size = 12)
male_height= []
female_height = []

for i in range(0, 12):
  if(genders[i] == 1):
    gauss[i] = np.random.normal(180, 7, None)
  else:
    gauss[i] = np.random.normal(167, 7, None)

for i in range(0, 12):
  if(genders[i] == 1):
    male_height.append(gauss[i])
  else:
    female_height.append(gauss[i])

plt.hist([male_height, female_height], color = ['blue', 'red'])
plt.legend(['Males', 'Females'])

male_avg = np.average(male_height)
female_avg = np.average(female_height)

plt.axvline(male_avg, color = 'black', linewidth = 2)
plt.axvline(female_avg, color = 'green', linewidth = 2)


