import cv2
from google.colab.patches import cv2_imshow

image = cv2.imread('/content/tiger.png')
image[:,:] += 60
cv2_imshow(image)
cv2.waitKey(0)
cv2.destroyAllWindows()
