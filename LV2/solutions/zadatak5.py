import matplotlib.pyplot as plt
import numpy as np

file=open("/content/mtcars.csv")
lines=[] 

for line in file:
    line=line.split(",")
    lines.append(line)

index_mpg=lines[0].index('"mpg"')
index_hp=lines[0].index('"hp"')
index_wt=lines[0].index('"wt"')

mpg=[]
hp=[]
wt=[]

for i in range (1,len(lines)):
    mpg.append(float(lines[i][index_mpg]))
    hp.append(float(lines[i][index_hp]))
    wt.append(float(lines[i][index_wt]))

print(mpg)
print(hp)

mpg_array=np.array(mpg)
hp_array=np.array(hp)
wt_array=np.array(wt)
colors = np.random.rand(3)
a=plt.scatter(mpg_array, hp_array,c='b',label='mpg-hp')
a1=plt.scatter(mpg_array, wt_array,c='c',label='mpg-wt')
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
          fancybox=True, shadow=True, ncol=4)
plt.show()
 
print("Min. vrijednost:",mpg_array.min())
print("Max. vrijednost:",mpg_array.max())
print("Sr. vrijednost:",round(mpg_array.mean(),2))
