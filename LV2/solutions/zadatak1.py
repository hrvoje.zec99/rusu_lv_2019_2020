import re

file = open('LV2/resources/mbox-short.txt', 'r')
all_emails = []
emails = []

for line in file:
  if line.startswith('Author:'):
    email = re.findall('\S*@\S+', line)
    all_emails.append(email[0])

for i in all_emails:
  if i not in emails:
    emails.append(i)

print(emails)

