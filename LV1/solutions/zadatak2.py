def getGrade(ocjena):
    slovo = 'A'
    if(ocjena >= 0.9):
        slovo  = 'A'
    elif(ocjena >= 0.8):
        slovo = 'B'
    elif(ocjena >= 0.7):
        slovo = 'C'
    elif(ocjena >= 0.6):
        slovo = 'D'
    else:
        slovo = 'F'
    return slovo


try:
    ocjena=float(input("unesi broj od 0.0 do 1.0:"))
    if((ocjena < 0.0) or (ocjena > 1.0)):
        print("ocjena izvan intervala")
    else:
        print(ocjena, getGrade(ocjena))
except:
    print("ocjena nije broj")