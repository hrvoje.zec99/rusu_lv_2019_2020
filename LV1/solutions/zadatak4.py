def avg(brojevi):
    return sum(brojevi)/len(brojevi)

lista_brojeva = []

while(1):
    unesi_broj=input("unesni broj: ")
    if(unesi_broj == "Done"):
        break
    try:
        lista_brojeva.append(int(unesi_broj))
    except:
        print("broj nije unesen")
        continue

print("broj unesenih brojeva: ", len(lista_brojeva))
print("min vrijednost: ", min(lista_brojeva))
print("max vrijednost: ", max(lista_brojeva))
print("srednja vrijednost: ", avg(lista_brojeva))
